package com.aceteam.barduino.model;

public class Content {

	/*
	 * Content is an ingredient and a quantity, drinks will be made of multiple contents
	 */
	private Ingredient i;
	private int quantity;
	
	public Content(Ingredient in, int x){
		i = in;
		quantity = x;
	}

	public String getIngredientName() {
		return i.toString();
	}

	public int getQuantity() {
		return quantity;
	}
	
	public String toString(){
		return i.toString() + " - " + quantity + " Measure(s)";
	}
	
	public void addQuantity(int i){
		quantity+=i;
	}

	public void removeQuantity(int i) {
		// TODO Auto-generated method stub
		if(quantity>0)
		quantity-=i;
	}
}
