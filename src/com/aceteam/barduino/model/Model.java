package com.aceteam.barduino.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

public class Model {

	/*
	 * Main Class for the back end program
	 */

	private static final String DELIMS = "/";
	private ArrayList<Drink> menu = new ArrayList<Drink>();
	private final static String address = "98:76:B6:00:35:79";
	private static final UUID MY_UUID = UUID.randomUUID();

	private BluetoothSocket btSocket = null;
	private OutputStream streamOut = null;

	public Model(InputStream is) {
		try {
			menu = populateMenu(is);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Model() {

	}

	private ArrayList<Drink> populateMenu(InputStream is) throws IOException {
		ArrayList<Drink> drinks = new ArrayList<Drink>();
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String line = br.readLine();
		while (line != null) {
			String[] s = line.split(DELIMS);
			Drink d = new Drink(s[0]);
			for (int i = 1; i < s.length; i += 2) {
				d.addIngredient(new Content(Ingredient.valueOf(s[i]), Integer
						.parseInt(s[i + 1])));
			}
			drinks.add(d);
			line = br.readLine();
		}
		br.close();
		return drinks;
	}

	public ArrayList<Drink> getDrinks() {
		return menu;
	}

	public Drink getDrink(String t) {
		Drink m = null;
		for (Drink d : menu) {
			if (d.getName().equals(t)) {
				m = d;
				break;
			}
		}
		return m;
	}

	// Used to connect phone to Arduino board
	private int connectToArduino(BluetoothAdapter bA) {
		// TODO Auto-generated method stub
		int status = 0;
		Log.d("Phone", address);
		BluetoothDevice device = bA.getRemoteDevice(address);
		Log.d("Phone", "Connecting with " + device);
		try {
			btSocket = device.createRfcommSocketToServiceRecord(MY_UUID);
		} catch (Exception e) {
			Log.e("", "Error creating socket");
			if(status == 0)
			status = 1;
		}

		try {
			btSocket.connect();
			Log.d("", "Connected");
		} catch (IOException e) {
			Log.e("", e.getMessage());
			try {
				Log.d("", "trying fallback...");

				btSocket = (BluetoothSocket) device
						.getClass()
						.getMethod("createRfcommSocket",
								new Class[] { int.class }).invoke(device, 1);
				btSocket.connect();

				Log.d("", "Connected");
			} catch (Exception x) {
				if(status == 0)
				status = 2;
			}
		}

		return status;
	}

	public int sendBluetooth(BluetoothAdapter bA, Drink d) {
		int status = 0;
		String send = "";
		for (Content c : d.getContents()) {
			if (c.getQuantity() > 0) {
				if (c.getIngredientName().equals("CURACAO")) {
					send = send.concat("B");
				} else {
					send = send.concat("" + c.getIngredientName().charAt(0));
				}
				send = send.concat("" + c.getQuantity());
			}
		}
		if (btSocket == null)
			status = connectToArduino(bA);
		try {
			streamOut = btSocket.getOutputStream();
		} catch (IOException e) {
			Log.d("Phone", "Output stream failed", e);
			if(status == 0)
			status = 3;
		}
		send = send.concat("X");
		Log.d("Sending", send);
		byte[] message = send.getBytes();

		try {
			streamOut.write(message);
		} catch (IOException e) {
			Log.d("Phone", "Sending failed", e);
			if(status == 0)
			status = 4;
		}
		return status;
	}

}
