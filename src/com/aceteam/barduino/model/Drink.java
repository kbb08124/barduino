package com.aceteam.barduino.model;

import java.util.ArrayList;

public class Drink {
	
	/*
	 * Drink is stored here, contains drink name and contents.
	 */

	private ArrayList<Content> contents = new ArrayList<Content>();
	private String name;
	
	public Drink(String title){
		name = title;
	}

	public ArrayList<Content> getContents() {
		return contents;
	}
	
	public ArrayList<String> getContentStrings(){
		ArrayList<String> list = new ArrayList<String>();
		for(Content c:contents){
			list.add(c.toString());
		}
		return list;
	}

	public void addIngredient(Content i){
		boolean found = false;
		for(Content c:contents){
			if(i.getIngredientName().equals(c.getIngredientName())){
				c.addQuantity(i.getQuantity());
				found = true;
				break;
			}
		}
		if(!found)
		contents.add(i); 
	}
	
	public void removeIngredient(Content i){
		for(Content c:contents){
			if(i.getIngredientName().equals(c.getIngredientName())){
				c.removeQuantity(i.getQuantity());
				break;
			}
		}
	}
	
	public String getName() {
		return name;
	}

	public String toString(){
		return name + contents;
	}
}
