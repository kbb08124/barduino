package com.aceteam.barduino.model;

/*
 * Enumerator for Ingredients to be used in drinks
 */
public enum Ingredient {
	VODKA, CURACAO, SCHNAPPS, RUM, CRANBERRY, ORANGE
}
