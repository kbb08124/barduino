package com.aceteam.barduino.view;

import java.util.ArrayList;

import com.aceteam.barduino.R;
import com.aceteam.barduino.model.*;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class CustomList extends Activity {
	
	ListView ings, chosen;
	Model m;
	Button send;
	int q = 0;
	Drink d = new Drink("Custom");
	
	//Variables for Picked Ingredients
	ArrayList<String> picked = new ArrayList<String>();
	ArrayAdapter<String> pickedIngs;
	private BluetoothAdapter mBluetoothAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.custom_list);
		m = new Model();
		chosen = (ListView) findViewById(R.id.lChosen);
		ings = (ListView) findViewById(R.id.lIngredients);
		send = (Button) findViewById(R.id.btSendCustom);
		setupIngredientList();
		setupPickedIngredients();

}

	private void setupPickedIngredients() {
		// TODO Auto-generated method stub
		for(int index = 0;index<6;index++){
			picked.add(d.getContents().get(index).toString());
		}
		pickedIngs = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,picked);
		chosen.setAdapter(pickedIngs);
		
		chosen.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				//Get index of item selected
				int itemPosition = position;
				
				//Get name of drink chosen
				String itemValue = (String) ings.getItemAtPosition(itemPosition);
				Ingredient i = Ingredient.valueOf(itemValue);
				d.removeIngredient(new Content(i,1));
				q = Math.max(0, q-1);
				picked.clear();
				for(int index = 0;index<6;index++){
					picked.add(d.getContents().get(index).toString());
				}
				pickedIngs.notifyDataSetChanged();
				chosen.setAdapter(pickedIngs);
			}
			
		});
	}

	private void setupIngredientList() {
		// TODO Auto-generated method stub		
		String[] values = new String[Ingredient.values().length];
		int index = 0;
		for(Ingredient i:Ingredient.values()){
			values[index] = i.name();
			d.addIngredient(new Content(i,0));
			index++;
		}
		
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, values);
		
		ings.setAdapter(adapter);
		
		ings.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				//Get index of item selected
				int itemPosition = position;
				
				//Get name of drink chosen
				String itemValue = (String) ings.getItemAtPosition(itemPosition);
				Ingredient i = Ingredient.valueOf(itemValue);
				if(q<8){
				d.addIngredient(new Content(i,1));
				q++;
				}
				else{
					Toast.makeText(getApplicationContext(), "Maximum measure reached", Toast.LENGTH_SHORT).show();
				}
				picked.clear();
				for(int index = 0;index<6;index++){
					picked.add(d.getContents().get(index).toString());
				}
				pickedIngs.notifyDataSetChanged();
				chosen.setAdapter(pickedIngs);
			}
			
		});
		
		send.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				checkBT();
			}
			
		});
	}
	
	//Check bluetooth is on and available
	private void checkBT(){
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if(!mBluetoothAdapter.isEnabled()){
			Toast.makeText(getApplicationContext(), "Bluetooth is not active", Toast.LENGTH_SHORT).show();
		}
		if(mBluetoothAdapter==null){
			Toast.makeText(getApplicationContext(), "Bluetooth is not available", Toast.LENGTH_SHORT).show();
		}
		int status = m.sendBluetooth(mBluetoothAdapter, d);
		switch (status) {
		case 1:
			Toast.makeText(getApplicationContext(),
					"Error creating Bluetooth socket", Toast.LENGTH_SHORT)
					.show();
			break;
		case 2:
			Toast.makeText(getApplicationContext(),
					"Error connecting to Barduino", Toast.LENGTH_SHORT)
					.show();
			break;
		case 3:
			Toast.makeText(getApplicationContext(),
					"Output stream failed", Toast.LENGTH_SHORT)
					.show();
			break;
		case 4:
			Toast.makeText(getApplicationContext(),
					"Sending to Barduino failed", Toast.LENGTH_SHORT)
					.show();
			break;
		default:
			Toast.makeText(getApplicationContext(),
					"Order sent!", Toast.LENGTH_SHORT)
					.show();
		}
	}

	}
