package com.aceteam.barduino.view;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.aceteam.barduino.R;
import com.aceteam.barduino.model.Content;
import com.aceteam.barduino.model.Drink;
import com.aceteam.barduino.model.Model;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Cocktails extends Activity {

	private ListView listView;
	private Model m;
	private TextView count, chosen, ings;
	private Button send;
	private BluetoothAdapter mBluetoothAdapter = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {
			InputStream is = getAssets().open("drinks.txt");
			m = new Model(is);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setContentView(R.layout.cocktail_list);
		count = (TextView) findViewById(R.id.tCocktails);
		chosen = (TextView) findViewById(R.id.tCocktailTitle);
		ings = (TextView) findViewById(R.id.tCocktailIngredients);
		send = (Button) findViewById(R.id.btSendCocktails);
		listView = (ListView) findViewById(R.id.list);

		ArrayList<Drink> drinks = m.getDrinks();
		count.setText(drinks.size() + " Drinks");

		String[] values = new String[drinks.size()];
		int index = 0;
		for (Drink d : drinks) {
			values[index] = d.getName();
			index++;
		}

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, values);

		listView.setAdapter(adapter);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				// Get index of item selected
				int itemPosition = position;

				// Get name of drink chosen
				String itemValue = (String) listView
						.getItemAtPosition(itemPosition);

				chosen.setText(itemValue);
				String ingredients = "";
				for (Content c : m.getDrink(itemValue).getContents()) {
					ingredients = ingredients.concat(c.toString() + "\n");
				}
				ings.setText(ingredients);
			}

		});

		send.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				checkBT();
			}

		});
	}

	// Check bluetooth is on and available
	private void checkBT() {
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (mBluetoothAdapter == null) {
			Toast.makeText(getApplicationContext(),
					"Bluetooth is not available", Toast.LENGTH_SHORT).show();
		}
		if (!mBluetoothAdapter.isEnabled()) {
			Toast.makeText(getApplicationContext(), "Bluetooth is not active",
					Toast.LENGTH_SHORT).show();
		}
		int status = m.sendBluetooth(mBluetoothAdapter,
				m.getDrink(chosen.getText().toString()));
		switch (status) {
		case 1:
			Toast.makeText(getApplicationContext(),
					"Error creating Bluetooth socket", Toast.LENGTH_SHORT)
					.show();
			break;
		case 2:
			Toast.makeText(getApplicationContext(),
					"Error connecting to Barduino", Toast.LENGTH_SHORT)
					.show();
			break;
		case 3:
			Toast.makeText(getApplicationContext(),
					"Output stream failed", Toast.LENGTH_SHORT)
					.show();
			break;
		case 4:
			Toast.makeText(getApplicationContext(),
					"Sending to Barduino failed", Toast.LENGTH_SHORT)
					.show();
			break;
		default:
			Toast.makeText(getApplicationContext(),
					"Order sent!", Toast.LENGTH_SHORT)
					.show();
		}
	}

}
