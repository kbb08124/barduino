package com.aceteam.barduino.view;

import com.aceteam.barduino.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class InitialChoice extends Activity implements OnClickListener {
	
	Button cocktails, custom;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choice);
		cocktails = (Button) findViewById(R.id.btCocktails);
		custom = (Button) findViewById(R.id.btCustom);
		
		cocktails.setOnClickListener(this);
		custom.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
		case R.id.btCocktails:
			Intent openCocktailList = new Intent("com.aceteam.COCKTAILS");
			startActivity(openCocktailList);
			break;
		case R.id.btCustom:
			Intent openCustomDrinks = new Intent("com.aceteam.CUSTOM");
			startActivity(openCustomDrinks);
			break;
		}
	}
	
	
}
