/*
  Barduino
 Serves drinks based upon orders via Bluetooth from associated Mobile App
 
 This code is property of the ACE team.
 */

#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_PWMServoDriver.h"
#include <Servo.h> 

//Initialise position of drinks
int curacao = 75;
int schnapps = 575;
int rum = 1075;
int vodka = 1585;
int cranberry = 2085;
int orange = 2620;
int pos = 0;

//Initialise Motor Board
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 

//Initialise actuator
Servo optics;
int opticPos = 70; //Set just below optic

//Initialise Motor Pulley
Adafruit_StepperMotor *pulley = AFMS.getStepper(200, 2);

//Used to clear buffer in case of multiple input
void serialFlush(){
  while(Serial.available() > 0) {
    char t = Serial.read();
  }
} 

//Method for vertical motion
void moveBoard(char c){
  Serial.println(c);
  //Read first element of string, drink type
  int movePos = 0;
  switch(c){ //Switch based upon what drink is required
  case 'B':
    movePos = curacao - pos;
    pos = curacao; //Position updates to current position
    break;
  case 'S':
    movePos = schnapps - pos;
    pos = schnapps;
    break;
  case 'R':
    movePos = rum - pos;
    pos = rum;
    break;
  case 'V':
    movePos = vodka - pos;
    pos = vodka;
    break;
  case 'C':
    movePos = cranberry - pos;
    pos = cranberry;
    break;
  case 'O':
    movePos = orange - pos;
    pos = orange;
    break;
  }
  //Forward = Left, Backward = Right
  int dir = BACKWARD; //Setup to move right by default
  pulley->setSpeed(70);
  //Move to required position
  if(movePos<0){ //If position is -ve, move left
    dir = FORWARD;
    pulley->setSpeed(60);
  }
  pulley->step(abs(movePos),dir,DOUBLE);
}

//Method for positioning
void actuate(int x){
  x = x-48;
  Serial.println(x);
  for(int i = 0; i<x; i++){ // Loop through actuation for each of x
    Serial.println(i+1);
    optics.write(106);
    delay(11500); //Time taken to pour a measure
    //    optics.write(85); //40s the new 0, 220s the new 180
    //    delay(5500);
    //    optics.write(98); //40s the new 0, 220s the new 180
    //    delay(5000);
    optics.write(opticPos);
    delay(8000);
  }
}

//The setup routine runs once when reset is pressed
void setup(){

  Serial.begin(9600); //set up Serial for Bluetooth input
  //Initialise position
  pos = 0;

  //Initialise Motor board and speed
  AFMS.begin(); //create with the default frequency 1.6kHz
  pulley->setSpeed(70); //Best balance of speed and stability

  optics.attach(10); //Sets up optics to run on Servo 1 port on Adafruit board
  optics.write(opticPos); //Ensures actuator is at start position
}

//The loop routine runs forever
void loop(){
  String content = ""; //Initialise variable to recieve input

  while(Serial.available()>0){ //Loop while serial has input
    char c = Serial.read(); //Read character in serial
    delay(2); // Required to allow Serial time to update
    if(c == 'X'){ //X is terminal character sent from phone
      break;
    } 
    content += c;
  }



  if(content.length()>0){//Check that input has been recieved
    Serial.println(content);
    //New loop for pouring drinks
    for(int i = 0; i<content.length(); i = i+2){
      moveBoard(content.charAt(i));
      actuate(content.charAt((i+1)));
    }
    //Return to start location
    pulley->step((pos+10),FORWARD,DOUBLE); //Moves by its total position back to start
  }
  pos = 0;
  serialFlush();
}






