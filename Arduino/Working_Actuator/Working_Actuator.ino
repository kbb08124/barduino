// Sweep
// by BARRAGAN <http://barraganstudio.com> 
// This example code is in the public domain.


#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include "utility/Adafruit_PWMServoDriver.h"
#include <Servo.h> 
 
 
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Servo myservo;  // create servo object to control a servo 
                // a maximum of eight servo objects can be created 
 
int pos = 0;    // variable to store the servo position 
 
void setup() 
{ 
  AFMS.begin();
  myservo.attach(10);  // attaches the servo on pin 9 to the servo object 
  myservo.write(108); //40s the new 0, 220s the new 180
  delay(15000);
  myservo.write(85); //40s the new 0, 220s the new 180
  delay(8000);
  myservo.write(100); //40s the new 0, 220s the new 180
  delay(7000);
  myservo.write(60); //40s the new 0, 220s the new 180
  delay(8000);
} 
 
 
void loop() 
{ 
 
//  for(pos = 0; pos < 180; pos += 1)  // goes from 0 degrees to 180 degrees 
//  {                                  // in steps of 1 degree 
//    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
//    delay(15);                       // waits 15ms for the servo to reach the position 
//  } 
//  for(pos = 180; pos>=1; pos-=1)     // goes from 180 degrees to 0 degrees 
//  {                                
//    myservo.write(pos);              // tell servo to go to position in variable 'pos' 
//    delay(15);                       // waits 15ms for the servo to reach the position 
//  } 
} 
